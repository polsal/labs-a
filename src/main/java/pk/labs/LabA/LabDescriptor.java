package pk.labs.LabA;

import pk.labs.LabA.Contracts.*;
import pk.labs.LabA.main.EkspresBean;
import pk.labs.LabA.controlpanel.ControlPanelBean;
import pk.labs.LabA.display.DisplayBean;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = DisplayBean.class.getName();
    public static String controlPanelImplClassName = ControlPanelBean.class.getName();

    public static String mainComponentSpecClassName = Ekspres.class.getName();
    public static String mainComponentImplClassName = EkspresBean.class.getName();
    // endregion

    // region P2
    public static String mainComponentBeanName = "main";
    public static String mainFrameBeanName = "app";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "silly";
    // endregion
}
